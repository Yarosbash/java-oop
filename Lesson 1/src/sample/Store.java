//1) Создайте пользовательский класс для описания товара (предположим, это задел для 
//интернет-магазина). В качестве свойств товара можете использовать значение цены, 
//описание, вес товара. Создайте пару экземпляров вашего класса и протестируйте их 
//работу.


package sample;

public class Store {

	public static void main(String[] args) {
		
		Staff staff1 = new Staff ("Помидор", "Красный", 0.1, 65);
		Staff staff2 = new Staff ("Помидор", "Желтый", 0.3, 75);
		Staff staff3 = new Staff ("Огурец", "Зеленый", 0.15, 80);
		
		System.out.println(staff1);
		System.out.println(staff2);
		System.out.println(staff3);

	}

}
