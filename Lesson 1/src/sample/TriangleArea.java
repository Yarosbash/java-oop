package sample;

public class TriangleArea {

	public static void main(String[] args) {		
		
		Triangle triangle = new Triangle(5.1, 6.2, 7.3);
		
		System.out.println("Площадь первого треугольника равна " + triangle.calculateTriangleArea());
		
		Triangle triangle1 = new Triangle();
		triangle1.setA(3.0);
		triangle1.setB(4.5);
		triangle1.setC(5.8);
		
		System.out.println("Площадь второго треугольника равна " + triangle1.calculateTriangleArea());
	}

}
