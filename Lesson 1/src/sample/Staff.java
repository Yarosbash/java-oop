package sample;

public class Staff {
	
		private String name;
		private String color;
		private double weight;
		private double price;
		
		public Staff(String name, String color, double weight, double price) {
			super();
			this.name = name;
			this.color = color;
			this.weight = weight;
			this.price = price;
		}		
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public double getWeight() {
			return weight;
		}

		public void setWeight(double weight) {
			this.weight = weight;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}

		@Override
		public String toString() {
			return "Staff [name=" + name + ", color=" + color + ", weight=" + weight + ", price=" + price + "]";
		}		
		
}
