package sample;

import java.util.Arrays;
import java.util.Comparator;

public class Group {

	private String groupName;
	private final Student[] students;
	
	public Group(String groupName, Student[] students) {
		super();
		this.groupName = groupName;
		this.students = students;
	}
		
	public Group () {
		super();
		students = new Student[10];
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	 public void addStudent(Student student) throws GroupOwerflowException {
		 for (int i = 0; i < students.length; i++) {
			if (students[i] == null) {
				students[i] = student;
				System.out.println(student.gender.getGetFilePermissions() + student.getName()+ " " + student.getLastName() + " " + " " + "добавлен в группу" + " " + student.getGroupName());
				return;
			}
		}
		
		throw new GroupOwerflowException(student.gender.getGetFilePermissions() + student.getName() + " " + student.getLastName() + " " + "не добавлен в группу" + " " + student.getGroupName());
	 }
	
	 public Student searchStudentByLastName(String lastName) throws StudentNotFoundException{
		 for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getLastName().equals(lastName)) {
					return students[i];
				}
			}
		}		 
		 throw new StudentNotFoundException("Такого студента нет в группе");
	 }
	 
	 public boolean removeStudentByID(int id) {
		 for (int i = 0; i < students.length; i++) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					System.out.println("Студент(-ка) с номером зачетки" + " " + id + " " + "удален из группы");
					return true;
				}
			}
		}
		 return false;
	 }
	 
	 @Override
		public String toString() {
			return "Группа [groupName=" + groupName + ", students=" + Arrays.toString(students) + "]";
		}

	 public void sortStudentsByLastName() {
	 	Arrays.sort(students, Comparator.nullsLast(new StudentComparator()));
	 	
	 	System.out.println();
	 	for (int i=0; i < students.length; i++) {
	 		System.out.println(students[i].getLastName() + " " + students[i].getName());
	 		
	 	}
	 }
}
