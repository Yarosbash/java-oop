package sample;

public class GroupOwerflowException extends Exception{

	public GroupOwerflowException() {
		super();		
	}

	public GroupOwerflowException(String message, Throwable cause) {
		super(message, cause);
	}

	public GroupOwerflowException(String message) {
		super(message);
	}

	public GroupOwerflowException(Throwable cause) {
		super(cause);
	}	
	
}
