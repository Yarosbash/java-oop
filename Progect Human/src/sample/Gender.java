package sample;

public enum Gender {

	Male ("Студент: "), Female("Студентка: ");
	
	private String getFilePermissions = " ";

	private Gender(String getFilePermissions) {
		this.getFilePermissions = getFilePermissions;
	}
	
	private Gender () {
		
	}

	public String getGetFilePermissions() {
		return getFilePermissions;
	}

	public void setGetFilePermissions(String getFilePermissions) {
		this.getFilePermissions = getFilePermissions;
	}

}