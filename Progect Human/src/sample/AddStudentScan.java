package sample;

import java.util.Scanner;

public class AddStudentScan {

	Scanner sc = new Scanner(System.in);
	
	public Student addStudent() {
	
		Student student = new Student (null, null, null, 0, null);
		
		System.out.println("Введите имя студента(-ки):");
		student.setName(sc.nextLine());
		
		System.out.println("Введите фамилию студента(-ки):");
		student.setLastName(sc.nextLine());
		
		System.out.println("Введите пол студента(-ки)(male или female):");
		String studentGender = (sc.nextLine());
		
		if (studentGender.equals("male")) {
			student.setGender(Gender.Male);
		} else if (studentGender.equals("female")) {
			student.setGender(Gender.Female);
		} else {
			System.out.println("Вы не указали пол студента(-ки)!");
		}
		
		System.out.println("Введите номер зачетки студента(-ки):");
		student.setId(sc.nextInt());
		
		System.out.println("Введите группу студента(-ки):");
		student.setGroupName(sc.nextLine());		
		
		return student;
	}
	
	public static void addScanStudentToGroup(Student student, Group group) {
		try {
			group.addStudent(student);
		} catch (GroupOwerflowException e) {
			System.out.println(student.gender.getGetFilePermissions() + student.getName() + " " + student.getLastName() + " " + "не добавлен в группу" + " " + student.getGroupName());
		}
	}
}
