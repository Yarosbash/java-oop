package sample;

public class Main {

	public static void main(String[] args) {
		
		Student student1 = new Student("Иван", "Иванов", Gender.Male, 1, "Первая");
		Student student2 = new Student("Алла", "Тригубенко", Gender.Female, 2, "Первая");
		Student student3 = new Student("Петр", "Петров", Gender.Male, 3, "Первая");
		Student student4 = new Student("Елена", "Чернявая", Gender.Female, 4, "Первая");
		Student student5 = new Student("Андрей", "Андреев", Gender.Male, 5, "Первая");
		Student student6 = new Student("Наталья", "Кучерова", Gender.Female, 6, "Первая");
		Student student7 = new Student("Сидор", "Сидоров", Gender.Male, 7, "Первая");
		Student student8 = new Student("Галина", "Красная", Gender.Female, 8, "Первая");
		Student student9 = new Student("Григорий", "Григорьев", Gender.Male, 9, "Первая");
		//Student student10 = new Student("Ирина", "Костенко", Gender.Female, 10, "Первая");
		Student student11 = new Student("Дмитрий", "Дмитренко", Gender.Male, 11, "Первая");

		Group first = new Group();
		
		try {
			first.addStudent(student1);
			first.addStudent(student2);
			first.addStudent(student3);
			first.addStudent(student4);
			first.addStudent(student5);
			first.addStudent(student6);
			first.addStudent(student7);
			first.addStudent(student8);
			first.addStudent(student9);
			//first.addStudent(student10);
			first.addStudent(student11);
			
		} catch (GroupOwerflowException e) {
			System.out.println();
			System.out.println(e.getMessage());
			System.out.println();
		}
		
		first.sortStudentsByLastName();	
		
		System.out.println();
		
		try {
			System.out.println(first.searchStudentByLastName("Григорьев"));
			System.out.println();
			System.out.println(first.searchStudentByLastName(student11.getLastName()));

		} catch (StudentNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println();

		first.removeStudentByID(3);
		first.removeStudentByID(8);
		
		Student student10 = new AddStudentScan().addStudent();
		AddStudentScan.addScanStudentToGroup(student10, first);
		
	}
}
