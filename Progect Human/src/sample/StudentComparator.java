package sample;

import java.util.Comparator;

public class StudentComparator implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		
		Student studentOne = (Student)o1;
		Student studentTwo = (Student)o2;
		
		String name1 = studentOne.getLastName();
		String name2 = studentTwo.getLastName();
		
		if (name1.compareTo(name2)>0) {
			return 1;
		}
		
		if (name1.compareTo(name2)<0) {
			return -1;
		}
		
		return 0;
	}

	
}
