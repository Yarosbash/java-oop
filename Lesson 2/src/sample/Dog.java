package sample;

public class Dog extends Animal{

	private String name;

	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public String getVoice() {
		return "Голос: Громкий, Гав - Гав";
	}
	
	public void eat() {
		System.out.println("Кушает: Неаккуратно");
	}
	 
	public void sleep() {
		System.out.println("Спит: Неспокойно" + "\n");
	}

	@Override
	public String toString() {
		return "Собака, Кличка " + name +", " + super.toString() + "";
	}
		
}
	
	
