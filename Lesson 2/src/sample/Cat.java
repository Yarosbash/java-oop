package sample;

public class Cat extends Animal{
	
	private String name;	

	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getVoice() {
		return ("Голос: Приятный, Мяу - Мяу");
	}
	
	public void eat() {
		System.out.println("Кушает: Аккуратно");
	}
	
	public void sleep(){
		System.out.println("Спит: тихо" + "\n");
	}

	@Override
	public String toString() {
		return "Кошка, Кличка " + name + ", " + super.toString() + "";
	}
	
	
}
