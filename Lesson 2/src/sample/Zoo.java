package sample;

public class Zoo {

	public static void main(String[] args) {
		
		Dog dog = new Dog("Кости", "Черный", 7, "Тузик");
		
		System.out.println(dog);
		System.out.println(dog.getVoice());
		dog.eat();
		dog.sleep();
		
		Cat cat = new Cat("Молоко", "Белая", 4, "Белка");
		
		System.out.println(cat);
		System.out.println(cat.getVoice());
		cat.eat();
		cat.sleep();
		
		Veterinarian veterinar = new Veterinarian("Павел Васильевич ");
		
		veterinar.treatment(cat);
		veterinar.treatment(dog);
	}

}
